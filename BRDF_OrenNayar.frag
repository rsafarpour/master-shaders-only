#version 150

//-----------------------------------------------------------------------------
//                                   INPUTS
//-----------------------------------------------------------------------------

smooth in struct Vertex
{
  vec3  normal;
  vec4  color;
  vec3  view_pos;
  float funcval;
  vec2  funcval_uv;
  vec2  label_uv;
  float label_no;
  float flag_nolabel;

} fs_in;

noperspective in vec3 vEdgeDist; //<! Fragment's barycenter coordinates.
flat in uint gInvertColor;       //<! Whether to invert the color, needed for
                                 //   color of normals/light vectors generated
                                 //   in geometry shader

//-----------------------------------------------------------------------------
//                                  CONSTANTS
//-----------------------------------------------------------------------------

const float WIREFRAME_THRESHOLD = 1.0/256.0;

// Render mode constants
const int RENDERCOLOR_MONO    = 0; //!< One color for all (vertex based).
const int RENDERCOLOR_RGB     = 1; //!< RGB Texturemap (per Vertex) typically supplied within a file.
const int RENDERCOLOR_FUNCVAL = 2; //!< Visualization of the vertices function values.
const int RENDERCOLOR_LABELS  = 3; //!< Visualization of the vertices labels.

// Fog  equation mode constants
const int FOG_EQUATION_LINEAR = 0;
const int FOG_EQUATION_EXP    = 1;
const int FOG_EQUATION_EXP2   = 2;

//-----------------------------------------------------------------------------
//                                  UNIFORMS
//-----------------------------------------------------------------------------

// Lighting model parameters
uniform vec3  lightdir_camfixed;      //!< Light direction for camera fixed light
uniform vec3  lightdir_worldfixed;    //!< Light direction for world fixed light
uniform float intensity_camfixed;     //!< Irradiance from camera fixed light
uniform float intensity_worldfixed;   //!< Irradiance from world fixed light
uniform float intensity_ambient;      //!< Irradiance from ambient light
uniform float roughness;              //!< Roughness parameter of Oren-Nayar model

// Render mode
uniform int  uRenderColor    = 0;     //!< Tells shader which color source to use (vertex, funcval, ...)
uniform bool flatShade       = false; //!< Smooth/Flat shading switch
uniform bool backCulling     = false; //!< Back face culling switch
uniform bool uEdgeShown      = false; //!< Enable wire frame on top of model
uniform bool uLabelSameColor = false; //!< Force uniform color for labels
uniform bool uIsoSolidFlag   = true;  //!< Whether to use defined solid color for isolines

// Ambient occlusion
uniform bool          withAO = false;
uniform sampler2DRect aoFactors;

// Color definitions
uniform sampler2D uFuncValTexMap;                                        //!< Texture with color scales
uniform sampler2D uLabelTexMap;                                          //!< Texturemap storing the label colors.
uniform vec4      colorSolidBack        = vec4( 0.5,  0.36, 0.36, 1.0 ); //!< Back face color if those aren't culled
uniform vec4      colorSolid            = vec4( 0.72, 0.72, 0.72, 1.0 ); //!< Color for solid color mode
uniform vec4      uEdgeColor            = vec4( 0.1,  0.1, 0.1, 1.0 );   //!< Color of wire frame lines
uniform vec4      uWireFrameColor       = vec4( 1.0,  1.0, 1.0, 1.0 );   //!< Color wire frame lines will fade out to (-> should be same as background)
uniform vec4      uLabelBorderColor     = vec4( 0.25, 0.25, 0.25, 1.0 ); //!< Color for connected components tagged as background
uniform vec4      uLabelBackgroundColor = vec4( 0.25, 0.25, 0.25, 1.0 ); //!< Color for connected components tagged as background
uniform vec4      uLabelNoColor         = vec4( 0.61, 0.09, 0.09, 1.0 ); //!< Unlabeled area color
uniform vec4      uLabelSingleColor     = vec4( 0.00, 0.00, 0.00, 1.0 ); //!< Label color if uLabelSameColor is true
uniform vec4      uIsoSolidColor        = vec4( 0.0, 0.0, 0.0, 1.0 );    //!< Isoline color

// Isoline parameters
uniform struct grFuncValIsoLineParams {
        bool  mIsoLinesShow;
        bool  mIsoLinesOnly;
        float mIsoDist;
        float mIsoOffset;
        float mIsoPixelWidth;
} funcValIsoLineParams;

// Fog parameters
uniform bool fogPresent = false;
uniform struct FogParameters {
        vec4  vFogColor; // Fog color
        float fStart;    // This is only for linear fog
        float fEnd;      // This is only for linear fog
        float fDensity;  // For exp and exp2 equation
        int   iEquation; // 0 = linear, 1 = exp, 2 = exp2 -- see defines GLSL_FOG_EQUATION_*
} fogParams;

//-----------------------------------------------------------------------------
//                                   OUTPUT
//-----------------------------------------------------------------------------

out vec4 final_color; //!< Final output color

//-----------------------------------------------------------------------------
//                                  FUNCTIONS
//-----------------------------------------------------------------------------

//! Floating point modulo operator
//! @param a Modulo dividend
//! @param b Modulo divisor
//! @returns a mod b
float modf(float a, float b)
{
    return a - b * floor( a / b );
}

//-----------------------------------------------------------------------------

//! Get intensity of edge line
//! depending on distance from edge
//! @returns Edge intensity in [0,1]
float edge_intensity()
{
    // determine frag distance to closest edge
    float edge_dist = min( min( vEdgeDist[0], vEdgeDist[1] ), vEdgeDist[2] );

    // -1.0 correlates to the width - the smaller the number the broader the line.
    return exp2( -1.0*edge_dist*edge_dist );

}

//-----------------------------------------------------------------------------

//! Get intensity of fog for current fragment
//! @returns Fog factor in [0,1]
float fog_factor() {

    float fragment_distance  = abs( fs_in.view_pos.z );
    float start              = fogParams.fStart;
    float end                = fogParams.fEnd;
    float density            = fogParams.fDensity;

    float factor = 0.0;
    if( fogParams.iEquation == FOG_EQUATION_LINEAR )
    {
        // Linearly map distance into [0,1]
        factor = (end-fragment_distance) / (end - start);
    }
    else if( fogParams.iEquation == FOG_EQUATION_EXP )
    {
        factor = exp(-density*fragment_distance );
    }
    else if( fogParams.iEquation == FOG_EQUATION_EXP2)
    {
        float e  = density*fragment_distance;
        float e2 = e * e;
        factor = exp(-e2);
    }
    factor = 1.0 - clamp( factor, 0.0, 1.0 );
    return factor;
}

//-----------------------------------------------------------------------------

float getIsoLineAlpha() {
        float isoDist    = funcValIsoLineParams.mIsoDist;
        float isoOffset  = funcValIsoLineParams.mIsoOffset;
        float pixelWidth = funcValIsoLineParams.mIsoPixelWidth;
        float funcValModIsoDist = modf(fs_in.funcval + isoOffset, isoDist );
        float funcValDxy = length( vec2( dFdx( fs_in.funcval ), dFdy( fs_in.funcval ) ) );

        float funcValRemainder = funcValModIsoDist - pixelWidth*funcValDxy;
        float funcValRemainderPos = funcValModIsoDist + pixelWidth*funcValDxy - isoDist;

        float alpha = 0.0;
        if( funcValRemainder <= 0.0 ) {
                alpha =  (-funcValRemainder/(pixelWidth*funcValDxy));
        }
        if( funcValRemainderPos >= 0.0 ) {
                alpha = (+funcValRemainderPos/(pixelWidth*funcValDxy));
        }

        return alpha;
}

//-----------------------------------------------------------------------------

vec3 get_normal()
{
    if( flatShade )
    {
        vec3 tangent  = dFdx( fs_in.view_pos.xyz );
        vec3 binormal = dFdy( fs_in.view_pos.xyz );

        return normalize(cross(tangent, binormal));
    }
    else
    {
        return fs_in.normal;
    }
}

//-----------------------------------------------------------------------------

//! Evaluates Oren-Nayar BRDF
//! @param color  Diffuse reflectance of surface
//! @param view   View vector
//! @param light  Light vector
//! @param normal Surface normal
//! @returns BRDF factor
vec3 brdf_orennayar(vec3 color, vec3 view, vec3 light, vec3 normal)
{
    view   = normalize(view);
    light  = normalize(light);
    normal = normalize(normal);

    vec3 v_proj   = view - normal * dot(normal, view); // View vector projected into normal plane
    vec3 l_proj   = light - normal * dot(light, view); // Light vector projected into normal plane
    float sigma_2 = roughness * roughness;
    float A       = 1 - 0.5 * (sigma_2)/(sigma_2 + 0.33);
    float B       = 0.45 * (sigma_2)/(sigma_2 + 0.09);

    float cos_phi     = clamp(dot(v_proj ,l_proj), 0.0f, 1.0);
    float theta_in    = acos(dot(normal, light));
    float theta_out   = acos(dot(normal, view));
    float max_angle   = max(theta_in, theta_out);
    float min_angle   = min(theta_in, theta_out);
    float brdf_factor = A + B * cos_phi * sin(max_angle) * tan(min_angle);

    // Should actually be color/PI * brdf_factor but
    // omitting PI here to brighten the final image:
    return color * brdf_factor;
}

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{ 
    if(!(gl_FrontFacing || backCulling))
    {
        // Cull back faces if culling is enabled
        discard;
    }
    else if(!gl_FrontFacing)
    {
        // Set back face color if
        // culling is disabled
        final_color  = colorSolidBack;
    }
    else
    {
        // Default full shading

        // Oren-Nayar model
        vec3 view = -fs_in.view_pos;
        vec3 color = fs_in.color.xyz;
        vec3 normal = get_normal();
        vec3 brdf_cam   = brdf_orennayar(color, view, lightdir_camfixed,   normal);
        vec3 brdf_world = brdf_orennayar(color, view, lightdir_worldfixed, normal);

        // Calculate light sources' reflectance assuming equal irradiance for R,G and B
        float cos_cam   = clamp(dot(normal, lightdir_camfixed), 0.0, 1.0);
        float cos_world = clamp(dot(normal, lightdir_worldfixed), 0.0, 1.0);
        vec3  c_dcam    = brdf_cam * intensity_camfixed * cos_cam;
        vec3  c_dworld  = brdf_world * intensity_worldfixed * cos_world;

        vec3 c_ambient = color * intensity_ambient;

        if (withAO)
        {
            float ao_factor = texture(aoFactors, gl_FragCoord.xy).x;
            c_ambient *= ao_factor;
        }

        final_color = vec4(c_ambient + c_dcam + c_dworld, 1.0);

        // Add wireframe lines on top of model
        if( uEdgeShown )
        {
            final_color = mix(final_color, uEdgeColor, edge_intensity() );
        }
    }




    // Shade color is determined, add post effects
    // Isolines
    if( funcValIsoLineParams.mIsoLinesShow )
    {
        float isoLineIntensity = getIsoLineAlpha();
        if( funcValIsoLineParams.mIsoLinesOnly )
        {
            if( isoLineIntensity == 0.0 )
            {
                discard;
            }
            if (gl_FrontFacing)
            {
                final_color = uIsoSolidColor;
            }
            final_color.a = isoLineIntensity;
        }
        else
        {
            vec4  isoLineColor = uIsoSolidColor;
            if( !uIsoSolidFlag )
            {
                float grayValInv = 1.0 - length( final_color.xyz );
               isoLineColor = vec4( grayValInv, grayValInv, grayValInv, 1.0 );
            }
            final_color = mix( final_color, isoLineColor, isoLineIntensity );
        }
    }


    // Fog
    if(fogPresent)
    {
        final_color = mix( final_color, fogParams.vFogColor, fog_factor());
    }


    // Invert color (done for normals and light vectors
    // generated in geometry shader)
    if( gInvertColor > 0u ) {
            final_color.rgb = 1.0 - final_color.rgb;
    }
}

