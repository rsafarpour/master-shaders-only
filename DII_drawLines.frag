#version 330

uniform sampler2DRect image; //<! Texture filtered image
uniform sampler2DRect depth; //<! Texture with z values

//-----------------------------------------------------------------------------
//                                  OUTPUT
//-----------------------------------------------------------------------------

out vec4 color; //!< Final output color

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    float value = texture(image, gl_FragCoord.xy).x;
    color = vec4(vec3(1) * value, 1.0);

    gl_FragDepth = texture(depth, gl_FragCoord.xy).y;
}
