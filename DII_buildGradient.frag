#version 330

//-----------------------------------------------------------------------------
//                                 UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect volume_ii; //<! Texture with view space depth values

//-----------------------------------------------------------------------------
//                                  OUTPUT
//-----------------------------------------------------------------------------

out vec4 gradient; //!< Final output color

//-----------------------------------------------------------------------------
//                                 FUNCTIONS
//-----------------------------------------------------------------------------

//! Sums up all components of a 3x3 matrix
//! @param   m  A 3x3 matrix
//! @returns Sum of all components of m
float sum(mat3 m)
{
    vec3 ones = vec3(1);
    vec3 tmp = vec3(0);
    tmp.x = dot(m[0], ones);
    tmp.y = dot(m[1], ones);
    tmp.z = dot(m[2], ones);
    return dot(tmp, ones);
}

//-----------------------------------------------------------------------------

//! Created gradient magnitude and orientation using the Scharr operator
//! @returns A vec2 with gradient magnitude in the x component and gradient
//!          orientation in the y component
vec2 scharr()
{
    // Scharr operator kernels
    mat3 S_x = mat3(3,0,-3, 10,0,-10, 3,0,-3); // Scharr oparator filter mask in x direction
    mat3 S_y = transpose(S_x);                 // Scharr oparator filter mask in y direction

    float vii_value[9];
    vii_value[0] = texture(volume_ii, gl_FragCoord.xy + vec2(-1.0, 1.0)).x - 1;
    vii_value[1] = texture(volume_ii, gl_FragCoord.xy + vec2( 0.0, 1.0)).x - 1;
    vii_value[2] = texture(volume_ii, gl_FragCoord.xy + vec2( 1.0, 1.0)).x - 1;

    vii_value[3] = texture(volume_ii, gl_FragCoord.xy + vec2(-1.0, 0.0)).x - 1;
    vii_value[4] = texture(volume_ii, gl_FragCoord.xy + vec2( 0.0, 0.0)).x - 1;
    vii_value[5] = texture(volume_ii, gl_FragCoord.xy + vec2( 1.0, 0.0)).x - 1;

    vii_value[6] = texture(volume_ii, gl_FragCoord.xy + vec2(-1.0, -1.0)).x - 1;
    vii_value[7] = texture(volume_ii, gl_FragCoord.xy + vec2( 0.0, -1.0)).x - 1;
    vii_value[8] = texture(volume_ii, gl_FragCoord.xy + vec2( 1.0, -1.0)).x - 1;

    mat3 vii_region = mat3(
                vii_value[0], vii_value[1], vii_value[2],
                vii_value[3], vii_value[4], vii_value[5],
                vii_value[6], vii_value[7], vii_value[8]);

    // Calculating gradient magnitude and orientation
    float G_x = sum(matrixCompMult(S_x, vii_region))/9.0;
    float G_y = sum(matrixCompMult(S_y, vii_region))/9.0;

    return vec2(sqrt(G_x*G_x + G_y*G_y), atan(G_y, G_x));
}

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    gradient.xy = scharr();
}
