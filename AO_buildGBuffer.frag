#version 330

//-----------------------------------------------------------------------------
//                                  INPUT
//-----------------------------------------------------------------------------

smooth in vec3  fragment_normal;
smooth in float view_depth;

//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

layout(location = 0) out vec4 normal_depth;

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    float depth  = gl_FragCoord.z;
    vec3  normal = fragment_normal;

    // Reverse normals of back facing primitives
    if( !gl_FrontFacing ) { normal = -normal_depth.rgb; }

    normal_depth.rgb = normal;
    normal_depth.a   = view_depth;
}
