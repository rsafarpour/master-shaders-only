#version 330

//-----------------------------------------------------------------------------
//                                 UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect filtered; //<! Texture with filtered depth

//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

out vec4 color; //!< Final output color

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    vec2  self = texture(filtered, gl_FragCoord.xy).xy; // x = v_r
    self.x -= 1.0;

    // Fetch direct neighbors
    float neighbors[4];
    neighbors[0] = texture(filtered, gl_FragCoord.xy - vec2(1.0, 0.0)).x - 1.0;
    neighbors[1] = texture(filtered, gl_FragCoord.xy - vec2(0.0, 1.0)).x - 1.0;
    neighbors[2] = texture(filtered, gl_FragCoord.xy + vec2(1.0, 0.0)).x - 1.0;
    neighbors[3] = texture(filtered, gl_FragCoord.xy + vec2(0.0, 1.0)).x - 1.0;

    // Find smallest absolute of neighbors with opposite sign
    float min_abs = 1.0/0.0; // == inf
    for(int i = 0; i < 4; i++)
    {
        float n = neighbors[i];
        if(sign(n) != sign(self.x) && abs(n) < min_abs)
        {
            min_abs = abs(n);
        }
    }

    // Color this fragment black if its absolute is larger than the
    // smallest absolute of different sign neighbors
    if(min_abs <= 1.0 && // == there are neighbors with different sign
       min_abs > abs(self.x))
    {
        color.x = 0.0;
    }
    else
    {
        color.x = 1.0;
    }
}
