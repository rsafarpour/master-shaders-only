#version 420
uniform layout(binding=0, r8ui) uimage1D hysteresis_flag; //<! Image texture indicating if another pixel
                                                          //   was added to the edge  by any fragment shader
uniform sampler2DRect last_iteration; //<! Texture with result from last iteration
uniform sampler2DRect minmax;         //<! Texture with minimum and maximum values in range [0,1]
uniform float         t_low;          //<! Low threshold for canny-like hysteresis
uniform float         t_high;         //<! High threshold for canny-like hysteresis

//-----------------------------------------------------------------------------
//                                  OUTPUT
//-----------------------------------------------------------------------------

out vec4 color; //!< Final output color

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    float old_neighbors[8];
    old_neighbors[0] = texture(last_iteration, gl_FragCoord.xy + vec2( 1.0,  0.0)).x;
    old_neighbors[1] = texture(last_iteration, gl_FragCoord.xy + vec2( 1.0,  1.0)).x;
    old_neighbors[2] = texture(last_iteration, gl_FragCoord.xy + vec2( 0.0,  1.0)).x;
    old_neighbors[3] = texture(last_iteration, gl_FragCoord.xy + vec2(-1.0,  1.0)).x;
    old_neighbors[4] = texture(last_iteration, gl_FragCoord.xy + vec2(-1.0,  0.0)).x;
    old_neighbors[5] = texture(last_iteration, gl_FragCoord.xy + vec2(-1.0, -1.0)).x;
    old_neighbors[6] = texture(last_iteration, gl_FragCoord.xy + vec2( 0.0, -1.0)).x;
    old_neighbors[7] = texture(last_iteration, gl_FragCoord.xy + vec2( 1.0, -1.0)).x;

    // Minmax values are in range [0,1], for thresholding
    // they are rescaled to [-1, 1]
    vec2  minmax_value = 2*texture(minmax, gl_FragCoord.xy).xy - vec2(1.0, 1.0);
    float old_value = texture(last_iteration, gl_FragCoord.xy).x;

    color.x = 1.0;

    // Keep old values
    if(old_value == 0.0)
    {
        color.x = 0.0;
    }

    // Mark all pixels above high threshold as edge,
    // also enforce another iteration, works because this
    // branch is taken during the first iteration only
    else if(minmax_value.x >  t_high ||
            minmax_value.y < -t_high)
    {
        color.x = 0.0;
        imageStore(hysteresis_flag, 0, uvec4(1));
    }



    // Mark all pixels above low threshold with a
    // marked neighbor, also enforce another iteration
    else if((minmax_value.x >  t_low ||
             minmax_value.y < -t_low)
            &&
            (old_neighbors[0] == 0.0 ||
             old_neighbors[1] == 0.0 ||
             old_neighbors[2] == 0.0 ||
             old_neighbors[3] == 0.0 ||
             old_neighbors[4] == 0.0 ||
             old_neighbors[5] == 0.0 ||
             old_neighbors[6] == 0.0 ||
             old_neighbors[7] == 0.0))

    {
        color.x = 0.0;
        imageStore(hysteresis_flag, 0, uvec4(1));
    }
}

