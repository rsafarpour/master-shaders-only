#version 150

layout (triangles) in;
layout (triangle_strip, max_vertices = 12) out; // 3 for the face itselt. +3 for normal. +2x3 for light directions.

//-----------------------------------------------------------------------------
//                                   INPUTS
//-----------------------------------------------------------------------------

smooth in struct Vertex
{
  vec3  normal;
  vec4  color;
  vec3  view_pos;
  float funcval;
  vec2  funcval_uv;
  vec2  label_uv;
  float label_no;
  float flag_nolabel;

} vs_out[];

flat out uint gInvertColor;

//-----------------------------------------------------------------------------
//                                  UNIFORMS
//-----------------------------------------------------------------------------

uniform highp mat4 modelview;  //! This is actually just the view matrix
uniform highp mat4 projection; //! Projection matrix

uniform vec3 uLightDirectionFixedCamera = vec3( 0.0, 0.0, 1.0 );
uniform vec3 uLightDirectionFixedWorld  = vec3( 0.0, 0.0, 1.0 );

uniform bool  uFaceNormals  = false;
uniform float uNormalLength = 1.5;
uniform float uNormalWidth  = 0.1;

uniform bool  uLightVectors    = true;
uniform int   uLightVecModVal  = 10;
uniform float uLightVeclLength = 20.0;
uniform float uLightVecWidth   = 0.005;

uniform vec2 uViewPortSize = vec2( 860, 718 ); // ( width, height ) of the viewport in pixel

//-----------------------------------------------------------------------------
//                                   OUTPUT
//-----------------------------------------------------------------------------

smooth        out Vertex fs_in;
noperspective out vec3 vEdgeDist; //! The face's barycenter coordinates.

//-----------------------------------------------------------------------------
//                                  FUNCTIONS
//-----------------------------------------------------------------------------

//! Emits primitives face
void generate_face()
{
    vEdgeDist = vec3(0.0f);

    // Edge/Wireframe Rendering - taken from 'Single-Pass Wireframe Rendering'
    //( http://www2.imm.dtu.dk/pubdb/views/edoc_download.php/4884/pdf/imm4884.pdf )
    vec2 p0 = uViewPortSize * gl_in[0].gl_Position.xy/gl_in[0].gl_Position.w;
    vec2 p1 = uViewPortSize * gl_in[1].gl_Position.xy/gl_in[1].gl_Position.w;
    vec2 p2 = uViewPortSize * gl_in[2].gl_Position.xy/gl_in[2].gl_Position.w;
    vec2 v0 = p2-p1;
    vec2 v1 = p2-p0;
    vec2 v2 = p1-p0;
    float area = abs( v1.x * v2.y - v1.y * v2.x );

    gInvertColor = 0u;

    // Pass thru the triangle as it is.
    for(int i = 0; i < gl_in.length(); i++ )
    {
        // According to http://www.opengl.org/wiki/Built-in_Variable_%28GLSL%29#Vertex_shader_outputs
        gl_Position                   = gl_in[i].gl_Position;
        gl_ClipDistance[0]            = gl_in[i].gl_ClipDistance[0];
        gl_ClipDistance[1]            = gl_in[i].gl_ClipDistance[1];
        gl_ClipDistance[2]            = gl_in[i].gl_ClipDistance[2];
        fs_in                         = vs_out[i];

        // +++ Edge/Wireframe Rendering
        if( i==0 ) {
            vEdgeDist = vec3( area/length(v0), 0.0, 0.0 );
        }
        if( i==1 ) {
            vEdgeDist = vec3( 0.0, area/length(v1), 0.0 );
        }
        if( i==2 ) {
            vEdgeDist = vec3( 0.0, 0.0, area/length(v2) );
        }
        EmitVertex();
    }
    EndPrimitive();
}

//! Emits primitives normal
//! @param cog    Center of gravity of the primitive
//! @param normal Normal of the primitive
void generate_normals(vec3 cog, vec3 normal)
{
        gInvertColor = 1u;
        normal *= uNormalLength;

        vec3 diagNormal = cross( normal, vec3( 0.0, 0.0, uNormalWidth ) );

        gl_Position = projection * vec4( cog, 1.0 );
        EmitVertex();

        vec4 sideShift = vec4( normal + diagNormal, 0.0 );
        gl_Position = projection * ( vec4( cog, 1.0 ) + sideShift );
        EmitVertex();

        sideShift = vec4( normal - diagNormal, 0.0 );
        gl_Position = projection * ( vec4( cog, 1.0 ) + sideShift );
        EmitVertex();

        EndPrimitive();
        gInvertColor = 0u;
}

//! Emits primitives light vectors for one light
//! @param cog       Center of gravity of the primitive
//! @param normal    Normal of the primitive
//! @param light_dir Light vector of light source
void generate_lightvectors(vec3 cog, vec3 normal, vec3 light_dir)
{
    gInvertColor = 1u;

    //vec3 lightDirFix = normalize( uLightDirectionFixedCamera );
    if( ( length( light_dir ) > 0.0 ) &&       // Show only if the light is turned on
        ( dot( normal, light_dir ) > 0.1 ) ) { // Show only if the light hits the face
            light_dir *= uLightVeclLength;

            // TODO: not stable if light_dir and vec3( 0.0, 0.0, uLightVecWidth )
            // are collinear
            vec3 diagNormal = cross( light_dir, vec3( 0.0, 0.0, uLightVecWidth ) );

            gl_Position = projection * vec4( cog, 1.0 );
            EmitVertex();

            vec4 sideShift = vec4( light_dir + diagNormal, 0.0 );

            gl_Position = projection * ( vec4( cog, 1.0 ) + sideShift );
            EmitVertex();

            sideShift = vec4( light_dir - diagNormal, 0.0 );
            gl_Position = projection * ( vec4( cog, 1.0 ) + sideShift );
            EmitVertex();

            EndPrimitive();
    }

    gInvertColor = 0u;
}

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    generate_face();

    // Calculate two vectors in the plane of the input triangle
    vec3 ab = vs_out[1].view_pos.xyz - vs_out[0].view_pos.xyz;
    vec3 ac = vs_out[2].view_pos.xyz - vs_out[0].view_pos.xyz;
    vec3 normal = normalize( cross( ab, ac ) );

    // Calculate the center of gravity of the triangle:
    vec3 cog = ( vs_out[0].view_pos.xyz + vs_out[1].view_pos.xyz + vs_out[2].view_pos.xyz ) / 3.0;

    if( uFaceNormals )
    {
        generate_normals(cog, normal);
    }
    if( uLightVectors &&
        (mod(gl_PrimitiveIDIn, uLightVecModVal) == 0))
    {
        vec3 light_camfixed = normalize( uLightDirectionFixedCamera );
        vec3 light_worldfixed = normalize( uLightDirectionFixedWorld);
        generate_lightvectors(cog, normal, light_camfixed);
        generate_lightvectors(cog, normal, light_worldfixed);
    }
}
