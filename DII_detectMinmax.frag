#version 330

#define DEG0     0
#define DEG22_5  0.3926991
#define DEG67_5  1.178097
#define DEG112_5 1.9634954
#define DEG157_5 2.7488936


//-----------------------------------------------------------------------------
//                                 UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect gradient;  //<! Texture with gradient
uniform sampler2DRect volume_ii; //<! Volume integral invariant

//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

out vec4 color; //!< Final output color

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    float volii_value = texture(volume_ii, gl_FragCoord.xy).x;
    float grad_dir = texture(gradient, gl_FragCoord.xy).y;

    float neighbors[2];
    // Fetch left and right neighbors if gradient is horizontal
    if( (grad_dir >= -DEG22_5 && grad_dir < DEG22_5) ||
         grad_dir >   DEG157_5 ||
         grad_dir <  -DEG157_5)
    {
        neighbors[0] = texture(volume_ii, gl_FragCoord.xy + vec2( 1.0, 0.0)).x;
        neighbors[1] = texture(volume_ii, gl_FragCoord.xy + vec2(-1.0, 0.0)).x;
    }


    // Fetch top and bottom neighbors if gradient is vertical
    else if( (grad_dir >= DEG67_5   && grad_dir < DEG112_5) ||
             (grad_dir >= -DEG112_5 && grad_dir < -DEG67_5))
    {
        neighbors[0] = texture(volume_ii, gl_FragCoord.xy + vec2(0.0,  1.0)).x;
        neighbors[1] = texture(volume_ii, gl_FragCoord.xy + vec2(0.0, -1.0)).x;
    }


    // Fetch top right and bottom left neighbors if gradient is first diagonal
    else if( (grad_dir >= DEG112_5 && grad_dir < DEG157_5) ||
             (grad_dir >= -DEG67_5 && grad_dir < -DEG22_5))
    {
        neighbors[0] = texture(volume_ii, gl_FragCoord.xy + vec2( 1.0,  1.0)).x;
        neighbors[1] = texture(volume_ii, gl_FragCoord.xy + vec2(-1.0, -1.0)).x;
    }

    // Fetch top left and bottom right neighbors if gradient is second diagonal
    else if( (grad_dir >= DEG22_5   && grad_dir < DEG67_5) ||
             (grad_dir >= -DEG157_5 && grad_dir < -DEG112_5))
    {
        neighbors[0] = texture(volume_ii, gl_FragCoord.xy + vec2( 1.0, -1.0)).x;
        neighbors[1] = texture(volume_ii, gl_FragCoord.xy + vec2(-1.0,  1.0)).x;

    }

    color.x = volii_value;
    color.y = volii_value;

    if(neighbors[0] >= volii_value || neighbors[1] >= volii_value)
    { color.x = 0.5; }
    if(neighbors[0] <= volii_value || neighbors[1] <= volii_value)
    { color.y = 0.5; }
}
