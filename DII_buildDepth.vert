#version 330

//-----------------------------------------------------------------------------
//                                 UNIFORMS
//-----------------------------------------------------------------------------

uniform highp mat4  modelview;  //<! Modelview  matrix
uniform highp mat4  projection; //<! Projection matrix
uniform       float view_near;  //<! Z value of near plane in view space
uniform       float view_far;   //<! Z value of far plane in view space

//-----------------------------------------------------------------------------
//                                  INPUT
//-----------------------------------------------------------------------------

in highp vec3  position; //<! Vertex position

//-----------------------------------------------------------------------------
//                                  OUTPUT
//-----------------------------------------------------------------------------

out float depth; //!< Z value in view space

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    vec4 view_position = modelview * vec4(position, 1.0);
    vec4 proj_position = projection * view_position;
    gl_Position        = proj_position;

    // Normalize depth to range [0,255]
    depth = 255*(view_position.z - view_near)/(view_far - view_near);
}
