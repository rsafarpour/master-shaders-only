#version 330

uniform sampler2DRect uFBO_Texture_ID; //<! Texture with g-buffer
uniform int           draw_normals;    //<! Flag whether to draw normals or depth
uniform float         view_near;       //<! Near plane z coordinate
uniform float         view_far;        //<! Far plane z coordinate

out vec4 color;

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    if (draw_normals == 1)
    {
        // Rescale normal to [0;1]
        vec3 normal = texture(uFBO_Texture_ID, gl_FragCoord.xy).rgb;
        normal = (normal + 1.0f)/2.0f;
        color  = vec4(normal, 1.0);
    }
    else
    {
        float depth = texture(uFBO_Texture_ID, gl_FragCoord.xy).a;

        // Remap depth to [0,1]
        depth = (depth - view_near)/(view_far - view_near);

        color = vec4(depth, depth, depth, 1.0);
    }
}
