#version 330

//-----------------------------------------------------------------------------
//                                 UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect depth;      //<! Texture with view space z values
uniform vec2          viewport;   //<! Viewport dimensions
uniform int           vr_radius;  //<! Radius of volume integral filter

//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

out vec4 color; //!< Final output color

//-----------------------------------------------------------------------------
//                                 FUNCTIONS
//-----------------------------------------------------------------------------

//! Calculates volume integral invariant
//! @param   radius  Radius of region to filter
//! @returns Volume integral invariant
float calc_vr(float radius)
{
    float r = radius;
    float r2 = r*r;
    float r3 = r2*r;

    float center = texture(depth, gl_FragCoord.xy).x;

    float sum  = 0;
    float total_volume = 0;
    for(float y = -r; y < r; y++)
        for(float x = -r; x < r; x++)
        {
            vec2 tex_coords = vec2(x,y) + gl_FragCoord.xy;
            float dist2 = x*x + y*y;
            if(dist2 <= r2)
            {
                float s_half = sqrt(r2 - dist2);
                float s_plus = 2.0 * s_half;
                float fragment = texture(depth, tex_coords).x;
                float h =  fragment - center + s_half;
                sum += max(min(h, s_plus), 0);
                total_volume += s_plus;
            }
        }

    // Following line should be: float vr = (2*sum/total_volume - 1);
    // but texture clamps this value for some reason, subtract 1 in next pass
    float vr = (2*sum/total_volume);
    vr = abs(vr - 1) < 0.001 ? 1 : vr;
    return vr/2;
}

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    color.x = calc_vr(vr_radius);
}
