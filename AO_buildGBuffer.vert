#version 330

//-----------------------------------------------------------------------------
//                                 UNIFORMS
//-----------------------------------------------------------------------------

uniform highp mat4 modelview;  //<! Modelview  matrix
uniform highp mat4 projection; //<! Projection matrix

//-----------------------------------------------------------------------------
//                                  INPUT
//-----------------------------------------------------------------------------

in highp vec3  position; //<! Vertex position
in highp vec3  vNormal;  //<! Vertex normal

//-----------------------------------------------------------------------------
//                                  OUTPUT
//-----------------------------------------------------------------------------


out highp vec3  fragment_normal; //<! Viewspace normal of fragment
out highp float view_depth;      //<! Viewspace depth (i.e. z-coordinate)

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    vec4 view_pos   = modelview * vec4(position, 1.0);
    fragment_normal = (modelview * vec4(vNormal, 0.0)).xyz;
    view_depth      = view_pos.z;
    gl_Position     = projection * view_pos;
}

