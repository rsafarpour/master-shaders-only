#version 330

#define MAX_KERNELSIZE 256
#define NOISE_X      4
#define NOISE_Y      4

//-----------------------------------------------------------------------------
//                                UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect gbuffer;                        //<! G-Buffer containing normals and viewspace depth
uniform vec3          kernelSamples[MAX_KERNELSIZE];  //<! Array with kernel samples set to MAX_KERNELSIZE
                                                      //<! because dynamic arrays are not available in GLSL
uniform vec2          noise[NOISE_X*NOISE_Y];         //<! Array of noise vectors for random rotation of kernel
uniform mat4          projection;                     //<! Projection matrix
uniform float         radius;                         //<! Desired radius of kernel
uniform vec2          viewport;                       //<! Viewport size
uniform bool          isOrthoMode;                    //<! Flag if orthographic/perspective mode is enabled
uniform int           kernelSize;                     //<! Actual number of samples in kernel
uniform float         fov;                            //<! Vertical field of view (fovy)
uniform float         aspect;                         //<! Screen aspect ratio
uniform float         contrast;                       //<! Contrast value

//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

out vec4 factor;

//-----------------------------------------------------------------------------
//                                FUNCTIONS
//-----------------------------------------------------------------------------

//! Transforms normalized device coordinates to viewspace
//! @param   ndc  Normalized device coordinates
//! @returns Viewspace coordinates of normalized device coordinates
vec3 toViewspace(vec2 ndc)
{
    vec2 pixel_location = ((ndc + 1.0f) / 2.0)*viewport;

    if(isOrthoMode)
    {        
        float hext_hori = 1/(projection[0][0]); // = half of the horizontal viewspace extent
        float hext_vert = 1/(projection[1][1]); // = half of the vertical viewspace extent
        return vec3(hext_hori * ndc.x,
                    hext_vert * ndc.y,
                    texture(gbuffer, pixel_location).a);
    }
    else
    {
        float thfov = tan(fov/ 2.0);
        vec3 view_ray = vec3(
                        ndc.x * thfov * aspect,
                        ndc.y * thfov,
                        -1.0);

        return (view_ray * -texture(gbuffer, pixel_location).a);
    }
}

//-----------------------------------------------------------------------------

//! Constructs a change of basis matrix which rotates a cartesian
//! coordinate system such that z is aligned with the surface normal
//! @returns A change of basis matrix
mat3 getTbnMatrix()
{
    // Get viewspace normal from g-buffer
    vec3 normal = texture(gbuffer, gl_FragCoord.xy).rgb;

    // Get noise vector from noise array
    uvec2 noise_dims   = uvec2(NOISE_X, NOISE_Y);
    uvec2 pixel_idx    = uvec2(gl_FragCoord.xy);
    uvec2 noise_coords = pixel_idx % noise_dims;
    uint  noise_idx    = noise_coords.x + noise_coords.y * noise_dims.x;

    // Construct change of basis matrix with noise vector
    // using Schmidt-Gram process to orthogonalise
    vec3 rnd = vec3(0);
    rnd.xy = noise[noise_idx];

    vec3 tangent  = normalize(rnd - (normal * dot(rnd, normal)));
    vec3 binormal = normalize(cross(normal, tangent));

    return mat3(tangent, binormal, normal);
}

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    int nSamples = min(kernelSize, MAX_KERNELSIZE);

    // Get fragment's view space position
    vec2 device_coords = gl_FragCoord.xy/viewport * 2.0f - 1.0f;
    vec3 origin = toViewspace(device_coords);

    mat3 tbn = getTbnMatrix();

    float occlusion = 0.0f;
    for(int i = 0; i < nSamples; ++i)
    {

        // Scale, reorient and place kernel sample
        vec3 sample_position = (tbn * kernelSamples[i]) * radius;
        sample_position += origin;

        // Project sample in screen space to get
        // g-buffer texture coordinates for sample
        vec4 proj_sample = projection * vec4(sample_position, 1.0);
        proj_sample /= proj_sample.w;
        proj_sample.xy = (proj_sample.xy * 0.5 + 0.5) * viewport;

        float gdepth = texture(gbuffer, proj_sample.xy).a;
        float sdepth = sample_position.z;

        // Compare stored g-buffer depth and sample depth
        if(sdepth <= gdepth /*&& abs(origin.z - gdepth) < radius*/)
        { occlusion += 1.0; }
    }

    // Sum up occlusion factors
    occlusion = occlusion / float(nSamples);
    factor.x = 1 - occlusion;

    // Increase contrast if desired
    factor.x = pow(factor.x, contrast);
}
