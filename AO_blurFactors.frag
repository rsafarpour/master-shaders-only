#version 330

#define SQRT_2PI 2.50662827463

//-----------------------------------------------------------------------------
//                                UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect factors;      //<! Texture with occlusion factors
uniform sampler2DRect gbuffer;      //<! G-Buffer texture
uniform float         blurRadius;   //<! Radius of pixels to be averaged
uniform vec2          viewport;     //<! Viewport dimensions

uniform float gauss_mu          = 0.0f;
uniform float gauss_sigma       = 2.5f;
uniform float d_depththreshold  = 10.0f;
uniform float d_normalthreshold = 0.1f;

//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

out vec4 color;

//-----------------------------------------------------------------------------
//                                FUNCTIONS
//-----------------------------------------------------------------------------

//! Evaluates a 1D gaussian normal distribution N(x|mu, sigma^2) with
//! mu == uniform variable gauss_mu and sigma == uniform variable gauss_sigma
//! @param   x  Normal distribution parameter
//! @returns N(x|mu, sigma^2)
float gauss(float x)
{
    float den = SQRT_2PI * gauss_sigma;
    float exp_num = - (x - gauss_mu);
    exp_num = exp_num*exp_num;
    float exp_den = 2 * gauss_sigma * gauss_sigma;

    return (1/den) * exp(-exp_num/exp_den);
}

//-----------------------------------------------------------------------------

//! Evaluates a 2D gaussian normal distribution similar to the 1D version
//! @param   xy  Normal distribution parameter
//! @returns N(x, y|mu, sigma^2)
float gauss(vec2 xy)
{
    vec2 tmp;
    tmp.x = gauss(xy.x);
    tmp.y = gauss(xy.y);
    return (tmp.x * tmp.y);
}

//-----------------------------------------------------------------------------

//! Calculate bilateral weight for blur process calculated by distance and normals
//! at two sample points, returns 0 if angle between normals is above a threshold
//! (defined by uniform variable d_normalthreshold) or distance in z direction is
//! too large (defined by uniform variable d_depththreshold). If weight is not 0
//! the value is determined by the gaussian weight of the distance in xy direction
//! @param   d_xy        xy distance vector of samples
//! @param   d_depth     z distance of samples
//! @param   dot_normals dot product of the normals at sample locations
//! @returns A weight value for bilateral filtering
float bilateral_weight(vec2 d_xy, float d_depth, float dot_normals)
{
    float result = gauss(d_xy);
    result *= 1 - step(d_depththreshold, d_depth);
    result *= step(d_normalthreshold, dot_normals);
    return result;
}

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{  
    // Define min/max of blur window
    vec2 zeros= vec2(0.0f, 0.0f);
    vec2 min = clamp(gl_FragCoord.xy - blurRadius, zeros, viewport);
    vec2 max = clamp(gl_FragCoord.xy + blurRadius, zeros, viewport);

    vec4 gcenter = texture(gbuffer, gl_FragCoord.xy);

    // Bilateral filtering:
    float total_weight = 0;
    float sum          = 0;
    for(float i = min.x; i < max.x; i++)
    {
        for(float j = min.y; j < max.y; j++)
        {
            vec4 gsample = texture(gbuffer, vec2(i,j));

            vec2  d_xy        = abs(vec2(i, j) - gl_FragCoord.xy);
            float d_depth     = abs(gcenter.a - gsample.a);
            float dot_normals = dot(gcenter.rgb, gsample.rgb);
            float weight      = bilateral_weight(d_xy, d_depth, dot_normals);

            sum += weight * texture(factors, vec2(i,j)).r;
            total_weight += weight;
        }
    }

    color.r = sum/total_weight;
}
