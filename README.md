This repo contains the shaders I contributed to [Gigamesh](https://gigamesh.eu) but
unfortunately haven't been merged into the master branch.

# SSAO Shaders

Files:

* AO_blurFactors.frag
* AO_buildGBuffer.vert
* AO_drawGBuffer.frag
* AO_drawFactors.frag
* AO_buildGBuffer.frag
* SSAO_buildFactors.frag
* AlchemyAO_buildFactors.frag

The files *AO_buildGBuffer.vert* and *AO_buildGBuffer.frag* set up a small G-Buffer containing depth  
and normal data. This G-Buffer is then consumed in a subsequent pass depending on activated AO effect.

If Starcraft2 AO is active the scene is drawn on a screen sized quad (shader not in this repo) and the  
fragment shader *SSAO_buildFactors.frag* is used. This shader calculates the ambient occlusion factors  
while rendering to a offscreen texture. This offscreen texture is then used in the shading step as a  
factor for reflected radiance.

If alchemy AO is active the file *AlchemyAO_buildFactors.frag* calculates the ambient occlusion factors  
while the rest of the process is exactly as for Starcraft2 AO.

The shaders *AO_drawGBuffer.frag*, *AO_drawFactors.frag* are for debugging and simply draw the G-Buffer  
and ambient occlusion factor textures respectively.
