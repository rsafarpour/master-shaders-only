#version 150

//-----------------------------------------------------------------------------
//                                   INPUTS
//-----------------------------------------------------------------------------

smooth in struct Vertex
{
  vec3  normal;
  vec4  color;
  vec3  view_pos;
  float funcval;
  vec2  funcval_uv;
  vec2  label_uv;
  float label_no;
  float flag_nolabel;

} fs_in;

noperspective in vec3 vEdgeDist; //<! Fragment's barycenter coordinates.
flat in uint gInvertColor;       //<! Whether to invert the color, needed for
                                 //   color of normals/light vectors generated
                                 //   in geometry shader

//-----------------------------------------------------------------------------
//                                  CONSTANTS
//-----------------------------------------------------------------------------

const float WIREFRAME_THRESHOLD = 1.0/256.0;

// Fog  equation mode constants
const int FOG_EQUATION_LINEAR = 0;
const int FOG_EQUATION_EXP    = 1;
const int FOG_EQUATION_EXP2   = 2;

//-----------------------------------------------------------------------------
//                                  UNIFORMS
//-----------------------------------------------------------------------------

// Render mode
uniform int  uRenderColor    = 0;     //!< Tells shader which color source to use (vertex, funcval, ...)
uniform bool flatShade       = false; //!< Smooth/Flat shading switch
uniform bool backCulling     = false; //!< Back face culling switch
uniform bool uEdgeShown      = false; //!< Enable wire frame on top of model
uniform bool uLabelSameColor = false; //!< Force uniform color for labels
uniform bool uIsoSolidFlag   = true;  //!< Whether to use defined solid color for isolines

// Ambient occlusion
uniform bool          withAO = false;
uniform sampler2DRect aoFactors;

// Color definitions
uniform sampler2D uFuncValTexMap;                                        //!< Texture with color scales
uniform sampler2D uLabelTexMap;                                          //!< Texturemap storing the label colors.
uniform vec4      colorSolidBack        = vec4( 0.5,  0.36, 0.36, 1.0 ); //!< Back face color if those aren't culled
uniform vec4      uEdgeColor            = vec4( 0.1,  0.1, 0.1, 1.0 );   //!< Color of wire frame lines
uniform vec4      uWireFrameColor       = vec4( 1.0,  1.0, 1.0, 1.0 );   //!< Color wire frame lines will fade out to (-> should be same as background)
uniform vec4      uIsoSolidColor        = vec4( 0.0, 0.0, 0.0, 1.0 );    //!< Isoline color

// Isoline parameters
uniform struct grFuncValIsoLineParams {
        bool  mIsoLinesShow;
        bool  mIsoLinesOnly;
        float mIsoDist;
        float mIsoOffset;
        float mIsoPixelWidth;
} funcValIsoLineParams;

// Fog parameters
uniform bool fogPresent = false;
uniform struct FogParameters {
        vec4  vFogColor; // Fog color
        float fStart;    // This is only for linear fog
        float fEnd;      // This is only for linear fog
        float fDensity;  // For exp and exp2 equation
        int   iEquation; // 0 = linear, 1 = exp, 2 = exp2 -- see defines GLSL_FOG_EQUATION_*
} fogParams;

//-----------------------------------------------------------------------------
//                                   OUTPUT
//-----------------------------------------------------------------------------

out vec4 final_color; //!< Final output color

//-----------------------------------------------------------------------------
//                                  FUNCTIONS
//-----------------------------------------------------------------------------

//! Floating point modulo operator
//! @param a Modulo dividend
//! @param b Modulo divisor
//! @returns a mod b
float modf(float a, float b)
{
    return a - b * floor( a / b );
}

//-----------------------------------------------------------------------------

//! Get intensity of edge line
//! depending on distance from edge
//! @returns Edge intensity in [0,1]
float edge_intensity()
{
    // determine frag distance to closest edge
    float edge_dist = min( min( vEdgeDist[0], vEdgeDist[1] ), vEdgeDist[2] );

    // -1.0 correlates to the width - the smaller the number the broader the line.
    return exp2( -1.0*edge_dist*edge_dist );

}

//-----------------------------------------------------------------------------

//! Get intensity of fog for current fragment
//! @returns Fog factor in [0,1]
float fog_factor() {

    float fragment_distance  = abs( fs_in.view_pos.z );
    float start              = fogParams.fStart;
    float end                = fogParams.fEnd;
    float density            = fogParams.fDensity;

    float factor = 0.0;
    if( fogParams.iEquation == FOG_EQUATION_LINEAR )
    {
        // Linearly map distance into [0,1]
        factor = (end-fragment_distance) / (end - start);
    }
    else if( fogParams.iEquation == FOG_EQUATION_EXP )
    {
        factor = exp(-density*fragment_distance );
    }
    else if( fogParams.iEquation == FOG_EQUATION_EXP2)
    {
        float e  = density*fragment_distance;
        float e2 = e * e;
        factor = exp(-e2);
    }
    factor = 1.0 - clamp( factor, 0.0, 1.0 );
    return factor;
}

//-----------------------------------------------------------------------------

float getIsoLineAlpha() {
        float isoDist    = funcValIsoLineParams.mIsoDist;
        float isoOffset  = funcValIsoLineParams.mIsoOffset;
        float pixelWidth = funcValIsoLineParams.mIsoPixelWidth;
        float funcValModIsoDist = modf(fs_in.funcval + isoOffset, isoDist );
        float funcValDxy = length( vec2( dFdx( fs_in.funcval ), dFdy( fs_in.funcval ) ) );

        float funcValRemainder = funcValModIsoDist - pixelWidth*funcValDxy;
        float funcValRemainderPos = funcValModIsoDist + pixelWidth*funcValDxy - isoDist;

        float alpha = 0.0;
        if( funcValRemainder <= 0.0 ) {
                alpha =  (-funcValRemainder/(pixelWidth*funcValDxy));
        }
        if( funcValRemainderPos >= 0.0 ) {
                alpha = (+funcValRemainderPos/(pixelWidth*funcValDxy));
        }

        return alpha;
}

//-----------------------------------------------------------------------------

vec3 get_normal()
{
    if( flatShade )
    {
        vec3 tangent  = dFdx( fs_in.view_pos.xyz );
        vec3 binormal = dFdy( fs_in.view_pos.xyz );

        return normalize(cross(tangent, binormal));
    }
    else
    {
        return fs_in.normal;
    }
}

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{ 
    if(!(gl_FrontFacing || backCulling))
    {
        // Cull back faces if culling is enabled
        discard;
    }
    else if(!gl_FrontFacing)
    {
        // Set back face color if
        // culling is disabled
        final_color  = colorSolidBack;
    }
    else
    {
        // Default shading, AO only
        vec3 c_ambient = vec3(1.0, 1.0, 1.0);
        if (withAO)
        {
            float ao_factor = texture(aoFactors, gl_FragCoord.xy).x;
            c_ambient *= ao_factor;
        }

        final_color = vec4(c_ambient, 1.0);

        // Add wireframe lines on top of model
        if( uEdgeShown )
        {
            final_color = mix(final_color, uEdgeColor, edge_intensity() );
        }
    }




    // Shade color is determined, add post effects
    // Isolines
    if( funcValIsoLineParams.mIsoLinesShow )
    {
        float isoLineIntensity = getIsoLineAlpha();
        if( funcValIsoLineParams.mIsoLinesOnly )
        {
            if( isoLineIntensity == 0.0 )
            {
                discard;
            }
            if (gl_FrontFacing)
            {
                final_color = uIsoSolidColor;
            }
            final_color.a = isoLineIntensity;
        }
        else
        {
            vec4  isoLineColor = uIsoSolidColor;
            if( !uIsoSolidFlag )
            {
                float grayValInv = 1.0 - length( final_color.xyz );
               isoLineColor = vec4( grayValInv, grayValInv, grayValInv, 1.0 );
            }
            final_color = mix( final_color, isoLineColor, isoLineIntensity );
        }
    }


    // Fog
    if(fogPresent)
    {
        final_color = mix( final_color, fogParams.vFogColor, fog_factor());
    }


    // Invert color (done for normals and light vectors
    // generated in geometry shader)
    if( gInvertColor > 0u ) {
            final_color.rgb = 1.0 - final_color.rgb;
    }
}

