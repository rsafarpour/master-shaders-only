#version 410

#define MAX_KERNELSIZE 256
#define NOISE_X      4
#define NOISE_Y      4
#define EPSILON      0.00001

//-----------------------------------------------------------------------------
//                                UNIFORMS
//-----------------------------------------------------------------------------

uniform sampler2DRect gbuffer;                       //<! G-Buffer containing normals and viewspace depth
uniform float         sigma;                         //<! Intensity scale
uniform float         k;                             //<! Contrast
uniform float         beta;                          //<! Bias (prevents self shadowing)
uniform vec2          kernelSamples[MAX_KERNELSIZE]; //<! Array with kernel samples set to MAX_KERNELSIZE
                                                     //<! because dynamic arrays are not available in GLSL
uniform vec2          noise[NOISE_X*NOISE_Y];        //<! Array of noise vectors for random rotation of kernel
uniform mat4          projection;                    //<! Projection matrix
uniform float         radius;                        //<! Desired radius of kernel
uniform vec2          viewport;                      //<! Viewport size
uniform bool          isOrthoMode;                   //<! Flag if orthographic/perspective mode is enabled
uniform int           kernelSize;                    //<! Actual number of samples in kernel
uniform float         fov;                           //<! Vertical field of view (fovy)
uniform float         aspect;                        //<! Screen aspect ratio


//-----------------------------------------------------------------------------
//                                 OUTPUT
//-----------------------------------------------------------------------------

out vec4 factor;

//-----------------------------------------------------------------------------
//                                FUNCTIONS
//-----------------------------------------------------------------------------

//! Transforms normalized device coordinates to viewspace
//! @param   ndc  Normalized device coordinates
//! @returns Viewspace coordinates of normalized device coordinates
vec3 toViewspace(vec2 ndc)
{
    vec2 pixel_location = ((ndc + 1.0f) / 2.0)*viewport;

    if(isOrthoMode)
    {
        float hext_hori = 1/(projection[0][0]); // = half of the horizontal viewspace extent
        float hext_vert = 1/(projection[1][1]); // = half of the vertical viewspace extent
        return vec3(hext_hori * ndc.x,
                    hext_vert * ndc.y,
                    texture(gbuffer, pixel_location).a);
    }
    else
    {
        float thfov = tan(fov/ 2.0);
        vec3 view_ray = vec3(
                        ndc.x * thfov * aspect,
                        ndc.y * thfov,
                        -1.0);

        return (view_ray * -texture(gbuffer, pixel_location).a);
    }
}

//-----------------------------------------------------------------------------

//! Transforms normalized device coordinates to viewspace in a plane z
//! @param   ndc  Normalized device coordinates
//! @param   z    distance from camera
//! @returns Viewspace coordinates of normalized device coordinates
vec3 toViewspace(vec2 ndc, float z)
{
    vec2 pixel_location = ((ndc + 1.0f) / 2.0)*viewport;

    if(isOrthoMode)
    {
        float hext_hori = 1/(projection[0][0]); // = half of the horizontal viewspace extent
        float hext_vert = 1/(projection[1][1]); // = half of the vertical viewspace extent
        return vec3(hext_hori * ndc.x,
                    hext_vert * ndc.y,
                    -z);
    }
    else
    {
        float thfov = tan(fov/ 2.0);
        vec3 view_ray = vec3(
                        ndc.x * thfov * aspect,
                        ndc.y * thfov,
                        -1.0);

        return (view_ray * z);
    }
}


//-----------------------------------------------------------------------------

//! Gets vector from noise texture
//! @param   pixel_position     Fragment coordinate
//! @returns                    Vector from noise texture
vec2 getNoiseVector(vec2 fcoord)
{
    uvec2 noise_dims   = uvec2(NOISE_X, NOISE_Y);
    uvec2 pixel_idx    = uvec2(fcoord);
    uvec2 noise_coords = pixel_idx % noise_dims;
    uint  noise_idx    = noise_coords.x + noise_coords.y * noise_dims.x;

    return noise[noise_idx];
}

//-----------------------------------------------------------------------------
//                                  MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    // Get viewspace normal from g-buffer
    vec3 n = texture(gbuffer, gl_FragCoord.xy).rgb;

    int nSamples = min(kernelSize, MAX_KERNELSIZE);

    float zPlane       = 0.0;

    // Get fragment's view space position
    vec2 device_coords = gl_FragCoord.xy/viewport * 2.0f - 1.0f;
    vec3 view_center   = toViewspace(device_coords);
    vec3 plane_center  = toViewspace(device_coords, zPlane);
    vec2 nvec          = getNoiseVector(gl_FragCoord.xy);

    float sum = 0.0f;
    for(int i = 0; i < nSamples; ++i)
    {               
        // Get viewspace sample position (in a plane parallel to screen plane)
        vec3 plane_sample = vec3(reflect(nvec, kernelSamples[i]), zPlane);
        plane_sample.xy = plane_sample.xy * radius;
        plane_sample += plane_center;

        // Project sample position back to screen
        // and store in normalized device coordinates
        vec4 proj_sample = projection * vec4(plane_sample, 1.0);
        proj_sample /= proj_sample.w;

        // Find sample position in view space at a distance
        // found in the G-buffer
        vec3 view_sample = toViewspace(proj_sample.xy);

        // Determine v for use in AO formula
        vec3 v = view_sample - view_center;
        float num = max(0, dot(v, n) + view_center.z*beta);
        float den = dot(v,v) + EPSILON;
        sum += num/den;
    }

    // Calculate occlusion factor via sum
    factor.x = pow(max(0, 1 - 2.0*sigma/nSamples * sum), k);
}
