#version 150

//-----------------------------------------------------------------------------
//                                   INPUTS
//-----------------------------------------------------------------------------

// Values vLabelID and vFlags use float instead of
// uint because Qt normalizes integer values
// see: http://qt-project.org/forums/viewthread/38929
in highp vec3  position;
in highp vec3  vNormal;
in highp vec4  vColor;
in       float vFuncVal;
in       float vLabelID;
in       float vFlags;

//-----------------------------------------------------------------------------
//                                  CONSTANTS
//-----------------------------------------------------------------------------

const uint  FLAG_LABELSENABLED = 0x08u;  //! Flag bit for enabled labels
const float TEXWIDTH_CSCALE    = 512.0f; //! Width of texture with color scales
const float TEXHEIGHT_CSCALE   = 512.0f; //! Height of texture with color scales
const float HEIGHT_CSCALE      = 10.0f;  //! Height of one color scale in texture
const float STRIDE_LABELSCALE  = 10.0f;  //! Stride of label color in color scale

//-----------------------------------------------------------------------------
//                                  UNIFORMS
//-----------------------------------------------------------------------------

uniform highp mat4 modelview;  //! This is actually just the view matrix
uniform highp mat4 projection; //! Projection matrix

// Information for scale matrix
uniform float uScaleHeight       = 1.0;
uniform float uScaleRadialBottom = 1.0;
uniform float uScaleRadialTop    = 1.0;

// Translation and rotation matrix
uniform highp mat4 uModelViewExtra = mat4( 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0
                                         );

uniform vec4 uClipPlane0 = vec4( 0.0, -1.0, 0.0, 0.0 ); // Classic clipping plane, provided by the plane of the Mesh.
uniform vec3 uClipBefore = vec3( 0.0, 0.0, 0.0 );       // Point in world coordinates sed when a single primitve is selected, than everything in front of it is clipped.

uniform bool  uFuncValInvert    = false;
uniform bool  uFuncValRepeat    = false;
uniform float uFuncValColorMap  = 1.0;
uniform float uFuncValIntervall = 10.0;
uniform float uFuncValMin;
uniform float uFuncValMax;
uniform float uFuncValLogGamma  =  1.0;

uniform float uFaceShiftViewZ  = 0.0;

uniform float uLabelTexMapSel       =  1.0; // Selected row within the texture map.
uniform float uLabelColorCount      = 11.0; // Number of colors available within the selected row of the texture map.
uniform float uLabelCountOffset     =  0.0; // Offset to shift the color with the row of the texture map.

//-----------------------------------------------------------------------------
//                                   OUTPUT
//-----------------------------------------------------------------------------

smooth out struct Vertex
{
  vec3  normal;       // Vertex normal
  vec4  color;        // Vertex color
  vec3  view_pos;     // Vertex position in vew space
  float funcval;      // Vertex funcval
  vec2  funcval_uv;   // Texture UVs for funcval mapping
  vec2  label_uv;     // Texture UVs for label mapping
  float label_no;     // Vertex' label ID
  float flag_nolabel; // Flag whether this vertex should be labeled or not

} vs_out;

//-----------------------------------------------------------------------------
//                                  FUNCTIONS
//-----------------------------------------------------------------------------

//! Floating point modulo operator
//! @param a Modulo dividend
//! @param b Modulo divisor
//! @returns a mod b
float modf(float a, float b)
{
    return a - b * floor( a / b );
}

//-----------------------------------------------------------------------------

//! Translates texel position to texture coordinates
//! @param texel_pos Position of texel
//! @returns Texture UV coordinates
vec2 to_texcoord(vec2 texel_pos)
{
    vec2 i = texel_pos;

    // For pixel i sample location s_d in dimension d
    // (with d in [0,N_d]) is: s_d =  (2i_d + 1)/(2N_d)
    // for both dimensions x and y:
    vec2 N = vec2(TEXWIDTH_CSCALE, TEXHEIGHT_CSCALE);
    return (2*i + 1)/(2*N);
}

//-----------------------------------------------------------------------------

//! Calculates texel position y for selected funcval color scale
//! @param texel_pos Position of texel
//! @returns Texel position in Y dimension for selected funcval color scale
float funcval_t(float selected_scale)
{
    float cscale_begin = HEIGHT_CSCALE*selected_scale;
    float cscale_center = cscale_begin + HEIGHT_CSCALE/2.0f;
    return cscale_center;
}

//-----------------------------------------------------------------------------

//! Calculates texel position x for funcval
//! @returns Texel position in X dimension for funcval
float funcval_s()
{
    float normalized_fval = ( vFuncVal - uFuncValMin ) / ( uFuncValMax - uFuncValMin );
    normalized_fval = clamp(normalized_fval, 0.0f, 1.0f);

    normalized_fval = pow(normalized_fval, uFuncValLogGamma );
    return (normalized_fval * TEXWIDTH_CSCALE);
}


//-----------------------------------------------------------------------------

//! Calculates texel position x for funcval while repeating the color scale,
//! for a growing funcval the x position moves forth and back over the color
//! scale. This is influenced by uFuncValIntervall
//! @returns Texel position in X dimension for funcval
float funcval_s_repeat()
{
    // Normalized s ranges from 0 -> 1 -> 0 for [0, uFuncValIntervall]
    float normalized_fval = 2 * mod( vFuncVal, uFuncValIntervall ) / uFuncValIntervall;
    if( normalized_fval > 1.0 ) {
            normalized_fval = ( -normalized_fval + 2.0 );
    }

    return (normalized_fval * TEXWIDTH_CSCALE);
}
//-----------------------------------------------------------------------------

//! Generates a scale matrix from uScaleRadialBottom and uScaleRadialTop
//! uniforms
//! @returns A scale matrix for this vertex
mat4 scale_matrix()
{
    float scale_radial = uScaleRadialBottom * ( 0.5 - position.z ) + uScaleRadialTop * ( 0.5 + position.z );
    mat4  scale_matrix = mat4(
                scale_radial, 0.0f,          0.0f,          0.0f,
                0.0f,         scale_radial,  0.0f,          0.0f,
                0.0f,         0.0f,          uScaleHeight,  0.0f,
                0.0f,         0.0f,          0.0f,          1.0f);


    return scale_matrix;

}

//-----------------------------------------------------------------------------

//! Gets texture coordinates for the texture holding the funcval color scales
//! @returns Texture UVs
vec2 funcval_texcoords()
{
    // Calculate color scale UV coordinates
    vec2 pixel_coord;
    pixel_coord.t = funcval_t(uFuncValColorMap);

    if ( uFuncValRepeat ) { pixel_coord.s = funcval_s_repeat(); }
    else                  { pixel_coord.s = funcval_s(); }

    vec2 funcval_uv = to_texcoord(pixel_coord);
    if(uFuncValInvert)
    {
        funcval_uv.s = 1 - funcval_uv.s;
    }

    return funcval_uv;
}

//-----------------------------------------------------------------------------

//! Calculates texel position y for selected label color scale
//! @param texel_pos Position of texel
//! @returns Texel position in Y dimension for selected label color scale
float label_t(float selected_scale)
{
    float cscale_begin = TEXHEIGHT_CSCALE - HEIGHT_CSCALE*selected_scale;
    float cscale_center = cscale_begin + HEIGHT_CSCALE/2.0f;
    return cscale_center;
}

//-----------------------------------------------------------------------------

//! Calculates texture UVs for this vertex' label
//! @returns Texture UVs
vec2 label_texcoords()
{
    float labelNrShifted = vLabelID + uLabelCountOffset;
    float labelIDMod = modf(labelNrShifted, uLabelColorCount);

    float labelTexCoord = (4.5 + 10.0*labelIDMod)/512.0;

    vec2 texel_coord;
    texel_coord.t = label_t(uLabelTexMapSel);
    texel_coord.s = (STRIDE_LABELSCALE/2 + STRIDE_LABELSCALE*labelIDMod);

    return to_texcoord(texel_coord);
}

//-----------------------------------------------------------------------------
//                                   MAIN
//-----------------------------------------------------------------------------

void main(void)
{
    // Compile the model matrix
    // uModelViewExtra contains translations and rotations!
    mat4 scale_matrix = scale_matrix();
    mat4 model_matrix = uModelViewExtra * scale_matrix;
    mat4 modelview    = modelview * model_matrix;

    // Set vertex position and Z offset
    vec4 viewpos   = modelview * vec4(position, 1.0);
    gl_Position    = projection * viewpos;
    gl_Position.z += uFaceShiftViewZ;

    // Set output vertex attributes
    vs_out.normal       = (modelview * vec4(vNormal, 0.0)).xyz;
    vs_out.color        = vColor;
    vs_out.view_pos     = viewpos.xyz;
    vs_out.funcval      = vFuncVal;
    vs_out.funcval_uv   = funcval_texcoords();
    vs_out.label_uv     = label_texcoords();
    vs_out.label_no     = vLabelID;
    vs_out.flag_nolabel = (float (uint(vFlags) & FLAG_LABELSENABLED)) * 1.0;

    // Set clipping planes
    vec3 modelPosition = (scale_matrix * vec4(position, 1.0)).xyz;
    gl_ClipDistance[0] = dot( uClipPlane0, uModelViewExtra * vec4( modelPosition, 1.0 ) );
}
